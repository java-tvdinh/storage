package com.tvdinh.demo.domain;

import com.tvdinh.demo.support.util.StringUtil;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.springframework.util.StringUtils;

import java.util.Objects;
import java.util.UUID;

@SuperBuilder
@Data
public class File {
    private String id;
    private String path;
    private String originalName;
    private String hashed;
    private Long size;
    private String type;
    private String ownerId;
    private String ownerType;
    private Boolean deleted;
    private Long version;
    private String downloadUrl;
    private String viewUrl;

    public static String genNewFileName(String originalName, String hashed) {
        if (!StringUtils.hasLength(hashed)) {
            hashed = UUID.randomUUID().toString();
        }
        String newFileName;
        String fullName = StringUtil.getSafeFileName(Objects.requireNonNull(originalName));
        String fileType = "";
        if (StringUtils.hasText(fullName)) {
            int last = fullName.lastIndexOf(".");
            if (last >= 0) {
                fileType = fullName.substring(last);
            }
        }
        newFileName = String.format("%s-%s%s", hashed, UUID.randomUUID().toString().substring(0, 8), fileType);
        return newFileName;
    }
}

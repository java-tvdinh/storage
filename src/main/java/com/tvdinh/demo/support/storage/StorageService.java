package com.tvdinh.demo.support.storage;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {

    String store(MultipartFile file);

    String store(MultipartFile filePart, String ownerFolderPath, String fileName);

    String store(MultipartFile filePart, String fileName);

    String store(File file);

    Path load(String filename);

    Resource loadAsResource(String filename);

    Stream<Path> loadAll();

    InputStreamResource download(String filename);
}

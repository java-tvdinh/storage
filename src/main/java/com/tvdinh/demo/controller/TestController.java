package com.tvdinh.demo.controller;

import com.tvdinh.demo.domain.File;
import com.tvdinh.demo.service.FileService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TestController {

    private final FileService fileService;

    public TestController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping("/files/uploads")
    ResponseEntity<List<File>> fileUploads(@RequestParam("files") List<MultipartFile> files,
                                           @RequestParam(value = "ownerId", required = false) String ownerId,
                                           @RequestParam(value = "ownerType", required = false) String ownerType) {
        List<File> res = fileService.createFile(files, ownerId, ownerType);
        return ResponseEntity.ok().body(res);
    }

    @GetMapping("/files/{id}/download")
    ResponseEntity<InputStreamResource> downloadFileById(@PathVariable(name = "id") String id) {
        return fileService.downloadFilePath(id);
    }

    @GetMapping("/files/{id}/view")
    ResponseEntity<?> viewFileById(@PathVariable(name = "id") String id) {
        Resource resource =  fileService.getFileResource(id);
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

}

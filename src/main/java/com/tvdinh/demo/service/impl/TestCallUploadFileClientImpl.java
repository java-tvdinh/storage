package com.tvdinh.demo.service.impl;

import com.tvdinh.demo.service.TestCallUploadFileClient;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Slf4j
@Service
public class TestCallUploadFileClientImpl implements TestCallUploadFileClient {
    private static final String calendarTimeZoneType = "SE Asia Standard Time";
    public static final String ICS = ".ics";


    @Override
    public void sendEmailInvitationAttend() {
        // Build attachment file
        MultipartFile multipartFileInvite = transferFileToMultipartFile(createFileInviteCalendar(null));
        // Upload file to StorageClient with params: MultipartFile
        String idFile = "";
        //String idFile = this.storageClient.fileUpload(multipartFileInvite, "", "").getData().getId();

        /**
         * @FeignClient(name = "storage", fallbackFactory = StorageClientFallback.class)
         * @PostMapping(value = "/api/files/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
         *     @LoadBalanced
         *     Response<FileDTO> fileUpload(@RequestPart(name = "file") MultipartFile file,
         *                                  @RequestParam(value = "ownerId", required = false) String ownerId,
         *                                  @RequestParam(value = "ownerType", required = false) String ownerType);
         */

        // Build content email using thymeleaf
        String content = "";
        // send email
    }

    private MultipartFile transferFileToMultipartFile(File file) {
        DiskFileItem fileItem = new DiskFileItem("file", "text/plain", false, file.getName(), (int) file.length(), file.getParentFile());
        try {
            InputStream inputStream = new FileInputStream(file);
            OutputStream outputStream = fileItem.getOutputStream();
            IOUtils.copy(inputStream, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        MultipartFile multipartFile = new CommonsMultipartFile(fileItem);

        return multipartFile;
    }

    private File createFileInviteCalendar(Objects meeting) {
        StringBuilder result = new StringBuilder();
        result.append("BEGIN:VCALENDAR\n")
                .append("PRODID:-//Microsoft Corporation//Outlook 16.0 MIMEDIR//EN\n")
                .append("METHOD:REQUEST\n")
                .append("VERSION:2.0\n")
                .append("X-MS-OLK-FORCEINSPECTOROPEN:TRUE\n")
                .append("BEGIN:VTIMEZONE\n")
                .append("TZID:").append(calendarTimeZoneType).append("\n")
                .append("BEGIN:STANDARD\n")
                .append("DTSTART:16010101T000000\n")
                .append("TZOFFSETFROM:+0700\n")
                .append("TZOFFSETTO:+0700\n")
                .append("END:STANDARD\n")
                .append("END:VTIMEZONE\n")
                // Event Time and location
                .append("BEGIN:VEVENT\n");
        // User attend
        Map<String, String> users = new HashMap<>();
        users.put("Test1", "test1@gmail.com");
        users.put("Test2", "test2@gmail.com");

        users.entrySet().forEach(it -> {
            result.append("ATTENDEE;CN=\"")
                    .append(it.getKey())
                    .append("\";RSVP=TRUE:mailto:")
                    .append(it.getValue()).append("\n");
        });

        String createdAt = "";
        String description = "";
        String finishAt = "";
        String startAt = "";
        String lastModifiedAt = "";
        String roomName = "";

        result.append("CLASS:PUBLIC\n")
                .append("CREATED:").append(createdAt).append("\n")
                .append("DESCRIPTION:").append(description).append("\n")
                .append("DTEND;TZID=\"").append(calendarTimeZoneType).append("\":")
                .append(finishAt).append("\n")
                .append("DTSTAMP:").append(Instant.now().toString()).append("\n")
                .append("DTSTART;TZID=\"").append(calendarTimeZoneType).append("\":")
                .append(startAt).append("\n")
                .append("LAST-MODIFIED:").append(lastModifiedAt).append("\n")
                .append("LOCATION:").append(roomName).append("\n");
        //Get PresiderUser nguoi to chuc
        String presiderUserEmail = "";
        result.append("ORGANIZER;CN=")
                .append(presiderUserEmail)
                .append(":mailto:").append(presiderUserEmail)
                .append("\n");

        String title = "";

        result.append("PRIORITY:5\n").append("SEQUENCE:0\n").append("SUMMARY;LANGUAGE=en-us:").append(title).append("\n")
                .append("TRANSP:OPAQUE\n").append("UID:").append(UUID.randomUUID()).append("\n")
                .append("X-MICROSOFT-CDO-BUSYSTATUS:TENTATIVE\n")
                .append("X-MICROSOFT-CDO-IMPORTANCE:1\n")
                .append("X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY\n")
                .append("X-MICROSOFT-DISALLOW-COUNTER:FALSE\n")
                .append("X-MS-OLK-AUTOSTARTCHECK:FALSE\n")
                .append("X-MS-OLK-CONFTYPE:0\n")
                // Remind before 15 minutes
                .append("BEGIN:VALARM\n")
                .append("TRIGGER:-PT15M\n")
                .append("ACTION:DISPLAY\n")
                .append("DESCRIPTION:Reminder\n")
                .append("END:VALARM\n")
                .append("END:VEVENT\n")
                .append("END:VCALENDAR");
        String resultFile = "TestFileSendEmail";
        File fileInvite = new File(System.getProperty("java.io.tmpdir") + "/" + resultFile + ICS);
        try {
            fileInvite.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(fileInvite);
            fileOutputStream.write(result.toString().getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return fileInvite;

    }
}

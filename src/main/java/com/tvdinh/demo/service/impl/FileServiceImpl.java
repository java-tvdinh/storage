package com.tvdinh.demo.service.impl;

import com.tvdinh.demo.config.StorageProperties;
import com.tvdinh.demo.domain.File;
import com.tvdinh.demo.service.FileService;
import com.tvdinh.demo.support.exception.BadRequestError;
import com.tvdinh.demo.support.exception.ResponseException;
import com.tvdinh.demo.support.storage.StorageService;
import com.tvdinh.demo.support.util.MD5Util;
import lombok.extern.slf4j.Slf4j;
import org.apache.tika.Tika;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
@Slf4j
public class FileServiceImpl implements FileService {

    private final StorageService storageService;
    private final StorageProperties properties;

    public FileServiceImpl(StorageService storageService, StorageProperties properties) {
        this.storageService = storageService;
        this.properties = properties;
    }

    @Override
    public List<File> createFile(List<MultipartFile> files, String ownerId, String ownerType) {
        validateFile(files);
        if ((!StringUtils.hasLength(ownerId)) || (!StringUtils.hasLength(ownerType))) {
            ownerType = "USER";
            Optional<String> optionalUserId = Optional.of(UUID.randomUUID().toString());
            ownerId = optionalUserId.orElse("ANONYMOUS");
        }
        List<File> fileUploads = new ArrayList<>();
        String finalOwnerType = ownerType;
        String finalOwnerId = ownerId;
        files.forEach(item -> {
            String originalName = StringUtils.cleanPath(Objects.requireNonNull(item.getOriginalFilename()));
            String path;
            String md5Hashed;
            Long version = 1L;
            try {
                md5Hashed = MD5Util.MD5(item.getBytes());
            } catch (IOException e) {
                log.error("Gen MD5 error", e);
                throw new ResponseException(BadRequestError.FILE_UPLOAD_INVALID);
            }
            String fileName = File.genNewFileName(originalName, md5Hashed);
            path = storageService.store(item, fileName);

            // create object -> add in list file
            File fileCmd = File.builder()
                    .originalName(originalName)
                    .path(path)
                    .size(item.getSize())
                    .type(item.getContentType())
                    .ownerId(finalOwnerId)
                    .ownerType(finalOwnerType)
                    .version(version)
                    .hashed(md5Hashed)
                    .build();

            fileUploads.add(fileCmd);
        });
        // save list file in db and return list file
        return fileUploads;
    }

    @Override
    public ResponseEntity<InputStreamResource> downloadFilePath(String id) {

        String pathFile = "2022/02/26/53/53ad52c35566ad73e5b38ab906907ade-b0d9bfec.pdf";
        String fileName = "BRD_MB -AMC - Buildings Management System_v1.0.pdf";

        InputStreamResource resource = storageService.download(pathFile);
        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        responseHeader.setAccessControlExposeHeaders(Collections.singletonList("Content-Disposition"));
        responseHeader.set("Content-disposition", "attachment; filename=" + fileName);
        return ResponseEntity.ok().headers(responseHeader)
                .body(resource);
    }

    @Override
    public Resource getFileResource(String id) {
        String pathFile = "2022/02/26/53/53ad52c35566ad73e5b38ab906907ade-b0d9bfec.pdf";
        return storageService.loadAsResource(pathFile);
    }

    private void validateFile(List<MultipartFile> multipartFiles) {
        if (CollectionUtils.isEmpty(multipartFiles)) {
            throw new ResponseException(BadRequestError.FILE_UPLOAD_INVALID);
        }
        var tika = new Tika();
        for (MultipartFile file : multipartFiles) {
            if (Objects.isNull(file)
                    || !StringUtils.hasLength(file.getOriginalFilename())
                    || Objects.equals(file.getOriginalFilename().trim(), "")) {
                throw new ResponseException(BadRequestError.FILE_UPLOAD_INVALID);
            }
            try {
                var mime = tika.detect(file.getInputStream());
                if (properties.getWhitelistMimeTypes() != null && properties.getWhitelistMimeTypes()
                        .stream().noneMatch(extension -> Objects.equals(extension.trim(), mime))) {

                    throw new ResponseException(BadRequestError.EXTENSION_FILE_UPLOAD_INVALID);
                }
            } catch (IOException e) {
                log.error("Cant get mine type", e);
                throw new ResponseException(BadRequestError.EXTENSION_FILE_UPLOAD_INVALID);
            }
        }
    }

}

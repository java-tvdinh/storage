package com.tvdinh.demo.service;

import com.tvdinh.demo.domain.File;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileService {

    List<File> createFile(List<MultipartFile> files, String ownerId, String ownerType);

    ResponseEntity<InputStreamResource> downloadFilePath(String id);

    Resource getFileResource(String id);

}
